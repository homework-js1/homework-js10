const tabs = document.getElementsByClassName('tabs-title');
let tabPanels;

window.onload=function() {
    tabPanels =document.querySelectorAll('.tabs-content');
    console.log(tabPanels);

    hideTabsContent(1);
}
function hideTabsContent(a) {
    for (let i = a; i < tabPanels.length; i++) {
        tabPanels[i].classList.remove('show');
        tabPanels[i].classList.add("hide");
    }
}

const ulTabs = document.querySelector('.tabs')
ulTabs.addEventListener("click", (event) => {
    let target=event.target;
    if (target.className === 'tabs-title') {
        for (let i = 0; i < tabs.length; i++) {
            if (target === tabs[i]) {
                showTabsContent(i);
                break;
            }
        }
    }

});

function showTabsContent(b){
    if (tabPanels[b].classList.contains('hide')) {
        hideTabsContent(0);
        tabPanels[b].classList.remove('hide');
        tabPanels[b].classList.add('show');
    }
}